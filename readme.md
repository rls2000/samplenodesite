# Step 1:
Go to [http://nodejs.org/](http://nodejs.org/) and install

# Step 2:
Once installed open a command promote as ADMIN
Type:
``` DOS
npm install -g express && npm install -g grunt-cli

```

# Step 3:
Once that has finished.  Download the [https://bitbucket.org/rls2000/samplenodesite](https://bitbucket.org/rls2000/samplenodesite)

# Step 4:
Open a command promote as ADMIN
Type:
``` DOS
cd C:/WhereYouDownloadedTheFolder/Sample
```

# Step 5:
Now you need to install all the extras that the website needs
Type:
``` DOS
npm install
```

# Step 6:
Run the site!!!
Type:
``` DOS
grunt
```
[http://localhost:3000](http://localhost:3000)


# Reference:
* How to write css in less. [http://lesscss.org/](http://lesscss.org/)
* How to write html in jade. [http://jade-lang.com/](http://jade-lang.com/)
* How does expressjs work. [http://expressjs.com/](http://expressjs.com/)
* How does nodejs work. [http://nodejs.org/](http://nodejs.org/)
* Get cool plug-in for nodejs. [https://npmjs.org/](https://npmjs.org/)
* How does gruntjs work. [http://gruntjs.com/](http://gruntjs.com/)

* A good playground to learn jade and less [http://codepen.io/](http://codepen.io/)