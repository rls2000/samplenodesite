function getScript(a, b) {
    var c = document.createElement("script");
    c.src = a;
    var d = document.getElementsByTagName("head")[0], e = !1;
    c.onload = c.onreadystatechange = function() {
        e || this.readyState && "loaded" !== this.readyState && "complete" !== this.readyState || (e = !0, 
        b(), c.onload = c.onreadystatechange = null, d.removeChild(c));
    }, d.appendChild(c);
}

function loadData() {
    rlsSearch.initialize();
}

var rlsSearch = function() {
    "use strict";
    var a = {}, b = "images/", c = "javascripts/select2.js", d = function(a) {
        return a.id ? a.text : a.text + "<img class='flag' src='" + b + a.text + "_county.png' style='float:right;'/>";
    }, e = function() {
        $.ajax({
            url: "//cd.mlspropertyfinder.com/api/search/getlisttowns/?id=nothing&state=MA",
            dataType: "jsonp",
            type: "get",
            success: function(a) {
                $("#txttown").select2({
                    placeholder: "Select a city",
                    data: {
                        results: a,
                        more: !0
                    },
                    multiple: !0,
                    formatResult: d,
                    dropdownCssClass: "items"
                });
            }
        });
    };
    return a.initialize = e, a.select2file = c, a;
}();

!function() {
    "use strict";
}(), document.onreadystatechange = function() {
    if (console.log(rlsSearch.select2file), "complete" === document.readyState) try {
        getScript("//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.3.min.js", function() {
            getScript(rlsSearch.select2file, function() {
                loadData();
            });
        });
    } catch (a) {
        txt = "There was an error on this page.\n\n", txt += "Error description: " + a.message + "\n\n", 
        txt += "Click OK to continue.\n\n", alert(txt);
    }
};