var rlsSearch = (function() {
	"use strict";

	var exports = {},
		preload = "",
		//images = "/art/style/counties/",
		images = "images/",
		//code = "/art/style/lib/select2/select2.js",
		code = "javascripts/select2.js",


		preloadfn = function() {
			$.ajax({
				url: '//cd.mlspropertyfinder.com/api/search/getlisttowns/?id=nothing&state=MA',
				dataType: 'jsonp',
				type: "get",
				success: function(data) {
					console.log(data);
					preload = {
						results: data,
						more: true,
						text: "stuyff"
					};
				}
			});
		},


		formatdisplay = function(item) {
			if (!item.id) {
				return item.text + "<img class='flag' src='" + images + item.text + "_county.png' style='float:right;'/>";
			} else {
				return item.text;
			}
		},

		init = function() {
			//preloadfn();
			//console.log(preload);
			$.ajax({
				url: '//cd.mlspropertyfinder.com/api/search/getlisttowns/?id=nothing&state=MA',
				dataType: 'jsonp',
				type: "get",
				success: function(data) {
					$('#txttown').select2({
						placeholder: 'Select a city',
						data: {
							results: data,
							more: true
						},
						multiple: true,
						formatResult: formatdisplay,
						dropdownCssClass: "items"
						//formatSelection: formatdisplay

					});
				}

			});
		};
	exports.initialize = init;
	exports.select2file = code;
	return exports;

})();


(function() {
	"use strict";
	//rlsSearch.initialize();
}());


function getScript(url, success) {
	var script = document.createElement('script');
	script.src = url;
	var head = document.getElementsByTagName('head')[0],
		done = false;
	// Attach handlers for all browsers
	script.onload = script.onreadystatechange = function() {
		if (!done && (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')) {
			done = true;
			success();
			script.onload = script.onreadystatechange = null;
			head.removeChild(script);
		}
	};
	head.appendChild(script);
}

document.onreadystatechange = function() {
	console.log(rlsSearch.select2file);
	if (document.readyState === 'complete') {
		try {
			getScript("//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.0.3.min.js", function() {
				getScript(rlsSearch.select2file, function() {
					loadData();
				});
			});
		} catch (err) {
			txt = "There was an error on this page.\n\n";
			txt += "Error description: " + err.message + "\n\n";
			txt += "Click OK to continue.\n\n";
			alert(txt);
		}
	}
};

function loadData() {
	rlsSearch.initialize();
}