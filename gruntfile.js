module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		
		grunticon: {
			counties: {
				files: [{
					expand: true,
					cwd: 'private/svg',
					src: ['*.svg'],
					dest: 'public/images/counties'
				}]
			}
		},

		less: {
			// Compile all targeted LESS files individually

			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
				path: ["path"],
				stripBanners: true,
				compress: false
			},
			development: {
				files: [{
					expand: true,
					cwd: 'private/less',
					src: '*.less',
					dest: 'public/stylesheets/',
					ext: '.css'
				}]
			}
		},
		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				eqnull: true,
				browser: true,
				globals: {
					jQuery: true
				},
			},
			files: {
				src: ['private/js/*.js']
			}
		},
		uglify: {
			options: {
				beautify: true
			},
			main: {
				files: [{
					expand: true,
					cwd: 'private/js/',
					src: '*.js',
					dest: 'public/javascripts/'
				}]
			}
		},
		copy: {
			select2: {
				files: {
					"public/stylesheets/select2.css": ['private/js/select2-3.4.5/select2.css'],
					"public/stylesheets/select2-bootstrap.css": ['private/js/select2-3.4.5/select2-bootstrap.css'],
					"public/javascripts/select2.js": ['private/js/select2-3.4.5/select2.js'],
					"public/stylesheets/select2.png": ['private/js/select2-3.4.5/select2.png'],
					"public/stylesheets/select2x2.png": ['private/js/select2-3.4.5/select2x2.png'],
					"public/stylesheets/select2-spinner.gif": ['private/js/select2-3.4.5/select2-spinner.gif'],
				}
			}
		},
		watch: {
			options: {
				livereload: true
			},
			js: {
				files: ['private/js/*.js'],
				tasks: ['jshint', 'uglify']
			},
			less: {
				files: ['private/less/**/*.less'],
				tasks: ['less']
			},
			jade: {
				files: ['views/*.jade'],
				tasks: []
			},
			select2: {
				files: ['private/js/select2-3.4.5/*.*'],
				tasks: ['copy:select2']
			}
		},
		nodemon: {
			dev2: {
				tasks: ['nodemon', 'watch'],
				options: {
					logConcurrentOutput: true
				}
			},
			dev: {
				options: {
					file: 'app.js',
					args: ['development'],
					nodeArgs: ['--debug'],
					ignoredFiles: ['README.md', 'node_modules/**'],
					watchedExtensions: ['js'],
					watchedFolders: [__dirname, 'routes', 'middle'],
					delayTime: 1,
					env: {
						PORT: '3000'
					},
					cwd: __dirname
				}
			}
		},
		concurrent: {
			target: {
				tasks: ['watch', 'nodemon:dev'],
				options: {
					logConcurrentOutput: true
				}
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('assemble-less');
	grunt.loadNpmTasks('grunt-nodemon');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-grunticon');
	// Default task(s).
	grunt.registerTask('default', ['concurrent']);

};