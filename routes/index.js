/*
 * GET home page.
 */

var fs = require('fs');

exports.index = function(req, res) {
	var MyData = {
		title: 'My Sample Site',
		Files: [],
		Counties: []
	},
	path = 'private/svg',
	color = "#5cb85c",
	imageWidth = 200,
	done = function() {
		//console.log(MyData);
		res.render('index.jade', MyData);
	},
	gcd = function(a,b) {
		if (b === 0)
			return a;
		return gcd(b, a % b);
	}
	;
//#5cb85c
	fs.readdir(path, 
		function(err, afiles) {
			if (err) console.log(err);
			var files = afiles.filter(
				function(file) {
					return file.substr(0,7) == 'Map_of_';
				}
			),
			data = "";
			for (var i = files.length - 1; i >= 0; i--) {
				var file = files[i],
					c = fs.readFileSync(path + '/' + file, 'utf-8'),
					text = '<use xlink:href="#state_outline" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />\n',
					counties = c.match(/(id="[a-z_\-]+")/gi),
					temp = "",
					find = '#state_outline" stroke="black" fill="white"',
					highlighted = '#state_outline" stroke="black" fill="' + color + '"'
				;
				MyData.Files.push(file);

				//MyData.Counties.push(counties);

				var w = c.match(/\swidth="[0-9\.]+"/)[0].match(/[0-9]+(?:\.[0-9]*)?/)[0];
				var h = c.match(/\sheight="[0-9\.]+"/)[0].match(/[0-9]+(?:\.[0-9]*)?/)[0];
				var StateName = file.replace("Map_of_","").replace('.svg', '');
				var w = Math.ceil(w);
				var h = Math.ceil(h);
				//var r = gcd(w,h);
				var r = imageWidth / w;
				var newH = Math.ceil(h * r);
				var newW = imageWidth;

				if (newH > imageWidth) {
					r = imageWidth / h;
					newH = imageWidth;
					newW = Math.ceil(w*r);
				}

				temp = '.' + StateName + ' {\n';
				temp += '\twidth: ' + newW + 'px;\n';
				temp += '\theight: ' + newH + 'px;\n';
				temp += '}\n';

				data += temp;



				// //console.log("Dim: " + w + "x" + h + "\nGcd: " + r + "\nNew Dim: " + imageWidth + ":" + newH + "\n");
				
				// For making the highlighted counties
					/*for (var j = counties.length - 1; j >= 0; j--) {

					//

						var newc = counties[j].replace('id="','').replace('"','');
						data = c.replace(find.replace("state_outline",newc),highlighted.replace('state_outline',newc));
						data = data.replace('#state_outline" stroke="black" fill="white"','#state_outline" stroke="black" fill="none"');
						data = data.replace(/\swidth="[0-9\.]+"/, ' width="' + newW + '"').replace(/\sheight="[0-9\.]+"/,' height="' + newH + '"');
//						var StateName = file.replace("Map_of_","").replace('.svg', '');
						var filename = file.replace("Map_of_","").replace('.svg', '_' + newc + '.svg');

						fs.writeFile(path + '/' + filename, data, 'utf-8', function(err) {
							console.log('It\'s saved!');
						});
					};*/
				
				

				/*
				/ For adding the "use" for each id.
					for (var j = counties.length - 1; j >= 0; j--) {
						temp += text.replace('state_outline',counties[j].replace('id="','').replace('"',''));
					};
					data = c.replace('</svg>', temp + '\n</svg>');
					fs.writeFile(path + '/' + file, data, 'utf-8', function(err) {
						console.log('It\'s saved!');
					});
				*/

					
			};
			fs.writeFile(path + 'states.less', data, 'utf-8', function(err) {
				console.log('i did it');
			});
			// files.forEach(
			// 	function(file) {
			// 		//console.log(file);
			// 		MyData.Files.push(file);
			// 		fs.readFile(path + '/' + file, 'utf-8', 
			// 			function(err, contents) {
			// 				if (err) console.log(err);
			// 				//console.log(contents.match(/(id="[a-z]+")/gi));
			// 				MyData.Counties.push(contents.match(/(id="[a-z]+")/gi));
			// 			}
			// 		);
			// 	}
			// );
			done();
		}
	);

	
};

/*<use xlink:href="#state_outline" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Barnstable" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Berkshire" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Bristol" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Dukes" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Essex" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Franklin" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Hampden" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Hampshire" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Middlesex" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Nantucket" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Norfolk" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Plymouth" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Suffolk" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<use xlink:href="#Worcester" stroke="black" fill="white" stroke-linejoin="round" stroke-width="50" />
<!--<use xlink:href="#Franklin" stroke="black" fill="green" stroke-linejoin="round" stroke-width="50" />-->
*/